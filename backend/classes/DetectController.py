import sys
import requests 
import threading
import time
import os
import json

import boto3
import cv2
import uuid
from decouple import config

from classes.PessoasController import PessoasController

AWS_ACCESS_KEY = config('AWS_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = config('AWS_SECRET_ACCESS_KEY')
REGION_NAME = config('REGION_NAME')
COLLECTION_ID = config('COLLECTION_ID')
LOG_ATIVO=config('LOG_ATIVO')
LOG_PATH=config('LOG_PATH')
IMAGE_NAME = "tmpimg.jpg"
FLAG_QUEUE = 1
PATH_TMP_IMGS = 'tmpimgs/'
API_ADDRESS = config('API_ADDRESS')

class DetectController:

    def __init__(self, camera):
        self.camera = camera
        self.start_aprovation_time = 0
        self.current_name =""
        self.sent_id = -1
        self.clear_names = False
        self.person = {}
        self.too_many_faces_status = False
        pass

    def iniciarProcessamento(self):    
        t1 = threading.Thread(target=self.processarCamera, daemon=True )
        t1.start()

    def processarCamera():
        buffer_inp = list()

        elapsed = int()
        start = timer()

        while self.camera.video.isOpened():        
            elapsed += 1
            frame = self.camera.image

            if frame is None:
                print ('\nEnd of Video')
                break

            buffer_inp.append(frame)
            
            # Only process and imshow when queue is full
            if elapsed % FLAG_QUEUE == 0:            
                self.processar()
                # Clear Buffers
                buffer_inp = list()

    def processar(self):

        status = True
        tempoAtual = time.time()
        realizouReconhecimento = False
        possuiFace = self.camera.possuiFace

           
        if ((tempoAtual - ultimoMomentoSemFace) > 5 and possuiFace and (tempoAtual - ultimaRequisicao) > 5):
            ultimaRequisicao = time.time()
            print(" # Detectando pessoa")
            response = ''

            try:
                cv2.imwrite(IMAGE_NAME, imagemOriginal)
                resultadoDeteccao, response = self.detectar()

                if (resultadoDeteccao):
    
                    externalString = response['FaceMatches'][0]['Face']['ExternalImageId']
                    pos = externalString.find('-')

                    if (pos != -1):
                        pessoa = externalString[:pos]
                        params = {'NRCPFFUNC': pessoa}
                        r = requests.post(API_URL+"pessoa", params)
                        r = r.json()

                        if (r['STATUS'] == True):
                            nomeCompleto = r['NMFUNC'].rstrip().split(' ')
                            pessoa = nomeCompleto[0] + " " + nomeCompleto[len(nomeCompleto)-1]
                            print (pessoa)

                    else:
                        pessoa = externalString
                    
                    realizouReconhecimento = True

                else:
                    raise Exception("Ocorreu um erro na detecção")

            except Exception as e:
                status = False
                print(str(e))

    def detectarPessoa(self):    
        t1 = threading.Thread(target=self.detectar, daemon=True )
        t1.start()

    def detectar(self):
        if self.camera.finish and self.camera.mode == 'detect':
            message = {
            "action": "start_recognition",
            "status": True,            
            }
            print(message)

            self.camera.finish = False
            startTime = time.time()
            threshold = 98
            maxFaces=50

            pessoas = []
            response = []
            status = True

            client = boto3.client(
                'rekognition',
                aws_access_key_id=AWS_ACCESS_KEY,
                aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                region_name=REGION_NAME,)

            with os.scandir(PATH_TMP_IMGS) as entries:
                for entry in entries:
                    try:                                    
                        imgfile = open(entry.path, 'rb')
                        imgbytes = imgfile.read()
                        imgfile.close()
                                
                        imgobj = {'Bytes': imgbytes}

                        result = client.search_faces_by_image(Image=imgobj, CollectionId=COLLECTION_ID, FaceMatchThreshold=threshold, MaxFaces=maxFaces)
                        
                        #faceDetails = client.detect_faces(Image=imgobj, Attributes=['DEFAULT'])
                        #result['FaceDetails'] = faceDetails

                        if (len(result['FaceMatches']) > 0):
                            externalString = result['FaceMatches'][0]['Face']['ExternalImageId']
                            pos = externalString.find('-')

                            if (pos != -1):
                                idPessoa = externalString[:pos]
                                pessoasController = PessoasController()
                                pessoa = pessoasController.findPessoa(idPessoa)
                                pessoasController.registrarVisita(idPessoa)
                                pessoas.append(pessoa)
                                result['person_id'] = idPessoa
                                result['person_data'] = pessoa
                            else:
                                pessoa = externalString

                        response.append(result)

                        if(self.str_to_bool(LOG_ATIVO)):
                            logFullPath = LOG_PATH+"/"+idPessoa+"-"+uuid.uuid4().hex
                            img = cv2.imread(entry.path, cv2.COLOR_BGR2RGB)
                            cv2.imwrite(logFullPath+".jpg", img)

                            with open(logFullPath+".json", 'w') as f:
                                json.dump(result, f)  

                    except Exception as e:
                        print(str(e))
                        status = False
                        break      
            
            if pessoas != [] and self.camera.mode == 'detect':
                current_persons = pessoas[0]
                current_person = current_persons['pessoa']
                print("Tempo Gasto para detectar -> "+str(time.time() - startTime))
                #print(current_person)
                self.send_detected_name(current_person)
            elif self.camera.mode == 'register':
                self.clear_detected_name()                        
            print("Tempo para aprovar ponto-> "+str(time.time() - startTime))
            self.camera.finish = True
            return status, response
    
    def str_to_bool(self, s):
        if s == 'True':
            return True
        elif s == 'False':
            return False        

    def too_many_faces(self,faces):
        if self.too_many_faces_status == True and len(faces) == 1:
            #self.start_aprovation_time = time.time()
            self.clear_detected_name()
            self.too_many_faces_status = False
        elif len(faces) > 1:
            message = {
                "action":  "too_many_faces",
                "status":  True
            }
            print(message)
            self.too_many_faces_status = True
    
    def send_detected_name(self,person):
        response = self.approve_recognition(person['id'])
        if response["status"] and person != None:
            message = {
            "action": "approve_recognition",
            "status": True,
            "name": person['nome'],
            "id": person['id'],
            "cpf":person['cpf'],
            "ultimaVisita":person['ultimaVisita'],
            "idsentido": response['sentido'],
            }
            print(message)
            
        else:
            message = {
            "action": "fail_aprovation",
            "name": person['nome'],
            "id": person['id'],
            "status": True
            }
            print(message)
           


    def clear_detected_name(self):             
        message = {
        "action": "clear_detected_name",
        "status":  True,        
        }
        print(message)
        self.clear_names = True

    def approve_recognition(self,person_id):     
        url = API_ADDRESS+'baterponto'

        data = {
            "CDFUNC": person_id,
        }
        r = requests.post(url = url, data = data) 
        result = r.json()      

        return result
        