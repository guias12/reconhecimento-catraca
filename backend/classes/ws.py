import threading
import requests 
import os
import traceback
import json
import numpy as np
import base64
import time

from flask import Flask, render_template, Response, request, make_response, current_app, jsonify
import cv2
from decouple import config
from datetime import datetime, timedelta
from functools import update_wrapper

from classes.camera import VideoCamera
from classes.PessoasController import PessoasController
from classes.rekognition.CollectionController import CollectionController

app = Flask(__name__)

HTTP_HOST = config('HTTP_HOST')
HTTP_PORT = config('HTTP_PORT')

camera = None

class WebServer:

    def __init__(self, camera_class):
        global camera
        camera = camera_class
        self.pessoas = PessoasController()
        self.collection = CollectionController()


    def crossdomain(origin=None, methods=None, headers=None, max_age=21600,
                attach_to_all=True, automatic_options=True):
        """Decorator function that allows crossdomain requests.
            Courtesy of
            https://blog.skyred.fi/articles/better-crossdomain-snippet-for-flask.html
        """
        if methods is not None:
            methods = ', '.join(sorted(x.upper() for x in methods))
        # use str instead of basestring if using Python 3.x
        if headers is not None and not isinstance(headers, basestring):
            headers = ', '.join(x.upper() for x in headers)
        # use str instead of basestring if using Python 3.x
        if not isinstance(origin, str):
            origin = ', '.join(origin)
        if isinstance(max_age, timedelta):
            max_age = max_age.total_seconds()

        def get_methods():
            """ Determines which methods are allowed
            """
            if methods is not None:
                return methods

            options_resp = current_app.make_default_options_response()
            return options_resp.headers['allow']

        def decorator(f):
            """The decorator function
            """
            def wrapped_function(*args, **kwargs):
                """Caries out the actual cross domain code
                """
                if automatic_options and request.method == 'OPTIONS':
                    resp = current_app.make_default_options_response()
                else:
                    resp = make_response(f(*args, **kwargs))
                if not attach_to_all and request.method != 'OPTIONS':
                    return resp

                h = resp.headers
                h['Access-Control-Allow-Origin'] = origin
                h['Access-Control-Allow-Methods'] = get_methods()
                h['Access-Control-Max-Age'] = str(max_age)
                h['Access-Control-Allow-Credentials'] = 'true'
                h['Access-Control-Allow-Headers'] = \
                    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
                if headers is not None:
                    h['Access-Control-Allow-Headers'] = headers
                return resp

            f.provide_automatic_options = False
            return update_wrapper(wrapped_function, f)
        return decorator
    

    def startServer(self):
        global HTTP_HOST
        global HTTP_PORT
        print(" [*] Starting WebServer.")
        app.run(host=HTTP_HOST, debug=False, port=HTTP_PORT)
        

    @app.route('/collections', methods=['POST', 'OPTIONS'])
    def getCollections():
        status = True

        try:

            collections = self.collection.listarCollection()

        except:
            status = False

        return jsonify(
            status = status,
            collections = collections
        )

    @app.route('/cadastro/collection', methods=['POST', 'OPTIONS'])
    def cadastroCollection():
        
        message = ""
        status = True

        try:

            name = request.json.get('name')

            if(name == None):
                raise Exception("Nome da collection não enviado. Parâmetro: name")        

            self.collection.criarCollection(name)

            message = "Collection criada com sucesso."
        except Exception as e:
            message = str(e)
            status = False

        return jsonify(
            status=status,
            message=message
        )

    @app.route('/delete/collection', methods=['POST', 'OPTIONS'])
    def deleteCollection():
        
        message = ""
        status = True
        collections = []

        try:

            nome = request.json.get('nome')

            if(nome == None):
                raise Exception("Nome da collection não enviado. Parâmetro: nome")        

            self.collection.apagarCollection(nome)

            message = "Collection apagada com sucesso."
            collections = self.collection.listarCollection()
        except Exception as e:
            message = str(e)
            status = False

        return jsonify(
            status=status,
            message=message,
            collections = collections
        )


    @app.route('/cadastro/pessoa/salvarnome', methods=['POST', 'OPTIONS'])
    def cadastroPessoaGetId():
        pessoas = PessoasController()
        message = ""
        status = True
        name = ""
        id = -1
        cod_func = -1
        
        try:
            cpf = request.json.get('cpf')
            if(cpf == None):               
                raise Exception("Cpf do usuário não enviado. Parâmetro: cpf")
            id = pessoas.gerarNovoId()
            cod_func = pessoas.get_cod_by_cpf(cpf)
            name = pessoas.get_person_name(cod_func)
            pessoas.armazenarNome(id,cod_func,name, cpf)

            message = "Nome salvo com sucesso."
        except Exception as e:
            message = str(e)
            status = False

        return jsonify(
            id=str(id),
            cod_func = cod_func,
            func_name=name,
            status=status,
            message=message
        )

    @app.route('/cadastro/pessoa/capturarfoto', methods=['POST', 'OPTIONS'])
    def cadastroPessoaCapturarFoto():
        pessoas = PessoasController()
        collection = CollectionController()
        message = ""
        status = True

        try:

            id = request.json.get('id')

            if(id == None):
                raise Exception("CPF do usuário não enviado. Parâmetro: cpf")

            path, label = pessoas.armazenarFoto(id, camera.image)

            resultado = collection.cadastrarFotos(path, label, id)

            if not resultado:
                raise Exception("Ocorreu um erro ao enviar a foto para AWS")


            message = "Imagem salva com sucesso."
        except Exception as e:
            message = str(e)
            status = False

        return jsonify(
            status=status,
            message=message
        ) 

    @app.route('/runaprovation', methods=['POST', 'OPTIONS'])
    def run_aprovation():    
        message = ""
        status = True
        try:
            imagBase64 =  request.json.get('photo')
            if(imagBase64 == None):
                raise Exception("Foto não enviada. Parâmetro: photo")       
            decoded_data = base64.b64decode(imagBase64)
            np_data = np.fromstring(decoded_data,np.uint8)
            image = cv2.imdecode(np_data,cv2.IMREAD_UNCHANGED)

            resultado = camera.get_frame(image)
            if not resultado:
                raise Exception("Falha na aprovação")
            
            message = "Aprovação concluida"
        
        except Exception as e:
            message = str(e)
            status = False
       
        return jsonify(
            status=status,
            message=message
        ) 

    @app.route('/video_feed', methods=['GET', 'OPTIONS'])
    def video_feed():

        global camera

        def gen(camera_videofeed):
            try:
                while True:
                    frame, _ = camera_videofeed.get_frame()
                    yield (b'--frame\r\n'
                        b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
            except Exception as e:
                print(str(e))
                print(traceback.format_exc())

        return Response(gen(camera),
                        mimetype='multipart/x-mixed-replace; boundary=frame')
    
    @app.route('/mode/<mode>', methods=['GET', 'OPTIONS'])
    @crossdomain(origin='*')
    def set_mode(mode):
        global camera

        # modes: register and detect
        if mode == 'register':
            camera.mode = 'register'
        elif mode == 'detect':
            camera.mode = 'detect'

        return jsonify({"status": True})

    @app.route('/getcod/<cpf>', methods=['GET', 'OPTIONS'])
    @crossdomain(origin='*')
    def get_cpf(cpf):
        result_response = {
            "status": True,
            "message": "Operação realizada com sucesso."
        }

        try:

            data = {
                "CPF": cpf
            }

            r = requests.post(url = 'http://briansilva1.zeedhi.com/workfolder/integracao-catraca/backend/service/index.php/getcodbycpf', data = data)
            result = r.json() 
            if(result['status']):
                result_response['CDFUNC'] = result['CDFUNC']        
            else:
                raise Exception(result['mensagem'])
        
        except Exception as e:
            result_response['status'] = False
            result_response['message'] = str(e)


        return jsonify(result_response)

    
        
    def main(self):    
        t1 = threading.Thread(target=self.startServer, daemon=True )
        t1.start()